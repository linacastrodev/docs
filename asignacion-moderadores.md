## Asignación de moderadores

| Dia     | Pista   | Horario     | Moderador |
| ------- | ------- | ----------- | --------- |
| Viernes | Pista I | 11:00-12:15 | Micael Gallego |
|                                                  |
| Viernes | Pista I | 12:30-13:45 | Pedro de las Heras |
|                                                      |
| Viernes | Pista I | 16:00-17:15 | Jesus González Barahona |
|                                                           |
| Viernes | Pista I | 17:30-18:45 | Juan González |
|                                                 |
| Sábado  | Pista I | 11:00-12:15 | Jose Maria Cañas Plaza |
|                                                          |
| Sábado  | Pista I | 12:30-13:45 | Álvaro del Castillo |
| Sábado  | Pista II | 12:30-13:45 | José Centeno |
|                                                 |
| Sábado  | Pista I | 16:00-17:15 | Gregorio Robles |
| Sábado  | Pista II | 16:00-17:15 | Patxi Gortázar|
|                                    |
| Sábado  | Pista I | 17:30-18:45 | Luis García Castro |
| Sábado  | Pista II | 17:30-18:45 | David Marzal |

## Asignación coordinadores técnicos

* José Ignacio Manso Llanes:
  - Viernes, 11:00-12:15: Pista I
  - Viernes, 12:30-final: Sala Derechos Digitales
  - Sábado, 11:00-final: Pista I
* María Campos:
  - Viernes, 11:00-13:45: Sala Cultura Libre
* Jorge Guerrero:
  - Viernes, 11:00-final: Pista II
* Rufino Garcia Sanchez:
  - Viernes, 12:30-final: Pista I
